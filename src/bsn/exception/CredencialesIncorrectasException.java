package bsn.exception;

public class CredencialesIncorrectasException extends Exception{
    public CredencialesIncorrectasException(String message) {
        super(message);
    }
}
