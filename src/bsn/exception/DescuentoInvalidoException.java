package bsn.exception;

public class DescuentoInvalidoException extends Exception{
    public DescuentoInvalidoException(String message) {
        super(message);
    }
}
