package bsn.exception;

public class UnidadesDeProductoInexistentesException extends Exception{

    public UnidadesDeProductoInexistentesException(String message) {
        super(message);
    }
}
