package bsn.exception;

public class NoHayProductosDisponiblesException extends Exception{
    public NoHayProductosDisponiblesException(String message) {
        super(message);
    }
}
