package bsn.exception;

public class ObjetoNoExisteException extends Exception{
    public ObjetoNoExisteException(String message) {
        super(message);
    }
}
