package bsn.exception;

public class EdadIncorrectaException extends Exception {
    public EdadIncorrectaException(String message) {
        super(message);
    }
}
