package bsn;

import dao.implementacionList.AdministradorDAOList;
import model.usuario.Administrador;
import model.usuario.Persona;

public class AdministradorBsn {

    private AdministradorDAOList administradorDAOList =new AdministradorDAOList();

    public Administrador getAdministrador(){
        return this.administradorDAOList.getAdministrador();
    }

    public  void setPersonaLogueada(Persona persona){
        administradorDAOList.setPersonaLogueada(persona);
    }
    public Persona getPersonaLogueada(){
        return Persona.getPersonaLogueada();
    }

}
