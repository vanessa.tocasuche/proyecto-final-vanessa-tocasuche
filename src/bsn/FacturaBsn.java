package bsn;

import dao.implementacionNio.FacturaDAONio;
import model.Factura;

public class FacturaBsn {


    //relacion
    private FacturaDAONio facturaDAO= new FacturaDAONio();


    //metodos
    public boolean nuevaFactura(Factura h){
        return facturaDAO.nuevaFactura(h);
    }

    public void nuevaVenta(){
        facturaDAO.nuevaVenta();
    }
    public Factura getFactura() {
        return facturaDAO.getFactura();
    }

    public int totalAPagar(String numeroDeFactura){
        return facturaDAO.totalAPagar(numeroDeFactura);
    }
    public int pagar(String numeroDeFactura){
        return facturaDAO.pagar( numeroDeFactura );
    }

}
