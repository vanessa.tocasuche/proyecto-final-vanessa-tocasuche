package bsn;

import bsn.exception.CredencialesIncorrectasException;
import bsn.exception.ObjetoYaExisteException;
import dao.EmpleadoNormalDAO;
import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.ClavesCredencialesIncorrectasException;
import dao.implementacionList.EmpleadoNormalDAOList;
import dao.implementacionNio.EmpleadoNormalDAONio;
import model.usuario.EmpleadoNormal;
import model.usuario.Persona;

import java.util.List;
import java.util.Optional;

public class EmpleadoNormalBsn {

    //private EmpleadoNormalDAO empleadoNormalDAO= new EmpleadoNormalDAOList();
    private EmpleadoNormalDAO empleadoNormalDAO= new EmpleadoNormalDAONio();

    public void registrarEmpleadoN(EmpleadoNormal empleadoN)throws ObjetoYaExisteException, EdadIncorrectaException{
        try{
            this.empleadoNormalDAO.registrarEmpleadoN(empleadoN);
        }catch (LlaveDuplicadaException lde){
            System.out.println(lde);
            throw new ObjetoYaExisteException( String.format( "El empleado con el ID: %s ya existe." , empleadoN.getId()) );
        }catch ( EdadIncorrectaException lde){
            System.out.println(lde);
            throw new EdadIncorrectaException( String.format( "Edad incorrecta, debe tener como minimo 18 años." ));
        }
    }

    public List<EmpleadoNormal> listarEmpleadosN() {
        return this.empleadoNormalDAO.listarEmpleadosN();
    }

    public void eliminarEmpleado(String id, String contrasenia) throws CredencialesIncorrectasException {
        try {
            this.empleadoNormalDAO.eliminarEmpleado( id, contrasenia);
        }catch (ClavesCredencialesIncorrectasException lde){
            System.out.println(lde);
            throw new CredencialesIncorrectasException(String.format( "Error con credenciales.") );
        }
    }
    public Optional<EmpleadoNormal> consultarEmpleadoPorId(String id){
        return empleadoNormalDAO.consultarEmpleadoPorId(id);
    }


    public void setEmpleadoLogueado(Persona persona){
        empleadoNormalDAO.setPersonaLogueada(persona);
    }
    public Persona getPersonaLogueada(){
        return Persona.getPersonaLogueada();
    }
}
