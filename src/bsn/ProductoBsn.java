package bsn;

import bsn.exception.DescuentoInvalidoException;
import bsn.exception.NoHayProductosDisponiblesException;
import bsn.exception.ObjetoYaExisteException;
import dao.ProductoDAO;
import dao.exception.DescuentoInvalidoDaoException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.NoSePuedeDescontarException;
import dao.implementacionNio.ProductoDAONio;
import model.Producto;

import java.util.List;

public class ProductoBsn {

    //private ProductoDAO productoDAO= new ProductoDAOList();
    private ProductoDAO productoDAO= new ProductoDAONio();

    public List<Producto> listarProductos(){
        return productoDAO.listarProductos();
    }


    public List<Producto> listarProductosConDescuentos(){
        return productoDAO.listarProductosConDescuento();
    }

    public void registrarProducto(Producto producto)throws ObjetoYaExisteException, DescuentoInvalidoException{
        try{
            this.productoDAO.registrarProducto(producto);
        }catch (LlaveDuplicadaException lde){
            System.out.println(lde);
            throw new ObjetoYaExisteException(String.format( "El producto con codigo: %s ya éxiste." , producto.getCodigo()) );
        }catch (DescuentoInvalidoDaoException die){
            System.out.println(die);
            throw new DescuentoInvalidoException(String.format( "El descuento debe estar entre 0 y 100." , producto.getCodigo()) );
        }
    }

    public void modificarProducto(Producto p){
        try{
            this.productoDAO.modificarProducto(p);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void eliminarProducto(String p){
        try{
            this.productoDAO.eliminarProducto(p);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void descontarProducto(String codigo) throws NoHayProductosDisponiblesException {
        try{
            productoDAO.descontarProducto(codigo);
        }catch (NoSePuedeDescontarException nde){
            System.out.println(nde);
            throw new NoHayProductosDisponiblesException(
                    String.format( "No hay unidades del producto con codigo: %s disponibles.", codigo));
        }
    }
    public void descontarProductos(String codigo, int i) throws NoHayProductosDisponiblesException{
        try{
            productoDAO.descontarProductos(codigo,i);
        }catch (NoSePuedeDescontarException nde){
            System.out.println(nde);
            throw new NoHayProductosDisponiblesException(
                    String.format( "No hay unidades del producto con codigo: %s disponibles.", codigo));
        }
    }

}
