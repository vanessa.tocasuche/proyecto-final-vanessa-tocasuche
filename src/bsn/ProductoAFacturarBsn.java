package bsn;

import dao.ProductoAFacturarDAO;
import dao.implementacionNio.ProductoAFacturarDAONio;
import model.Producto;
import model.ProductoAFacturar;

import java.util.List;
import java.util.Optional;

public class ProductoAFacturarBsn {

    private ProductoAFacturarDAO productoAFacturarDAO= new ProductoAFacturarDAONio();

    public void agregarUnidadProductoAFacturar(String numeroDeFactura, Producto producto){
        productoAFacturarDAO.agregarUnidadProductoAFacturar(numeroDeFactura, producto);
    }
    public void eliminarProductoAFacturar(String codigoProducto){
        productoAFacturarDAO.eliminarProductoAFacturar(codigoProducto);
    }
    public List<ProductoAFacturar> productosEnLaFactura(String numeroDeFactura){
        return productoAFacturarDAO.productosEnLaFactura(numeroDeFactura);
    }
    public Optional<ProductoAFacturar> optionalExisteNumDeFactura(String numeroDeFactura){
        return productoAFacturarDAO.optionalExisteNumDeFactura(numeroDeFactura);
    }


}
