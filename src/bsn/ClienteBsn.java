package bsn;

import bsn.exception.ClienteNoExisteException;
import bsn.exception.ObjetoYaExisteException;
import dao.ClienteDAO;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import dao.implementacionList.ClienteDAOList;
import dao.implementacionNio.ClienteDAONio;
import model.usuario.Cliente;
import model.usuario.Persona;

import java.util.List;
import java.util.Optional;

public class ClienteBsn {
    //private ClienteDAO clienteDAO= new ClienteDAOList();
    private ClienteDAO clienteDAO= new ClienteDAONio();

    public void registrarCliente(Cliente cliente) throws ObjetoYaExisteException {
        try {
            this.clienteDAO.registrarCliente(cliente);
        } catch (LlaveDuplicadaException lde) {
            System.out.println(lde);
            throw new ObjetoYaExisteException(String.format("El cliente con id: %s , ya existe.", cliente.getId()));
        }
    }

    public List<Cliente> listarClientes(){
        return this.clienteDAO.listarClientes();
    }

    public Optional<Cliente> consultarClientePorId(String id) throws ClienteNoExisteException {
        if (clienteDAO.consultarClientePorId(id).isPresent()){
            return clienteDAO.consultarClientePorId(id);
        }else{
            throw new ClienteNoExisteException(String.format( "El cliente con el id: %s , no existe.",id ));
        }
    }


    public void setClienteLogueado(Persona persona){
        clienteDAO.setPersonaLogueada(persona);
    }
    public Persona getPersonaLogueada(){
        return Persona.getPersonaLogueada();
    }

}
