package bsn;

import bsn.exception.ObjetoNoExisteException;
import dao.HistoriaDeProductoDAO;
import dao.exception.LlaveNoExisteException;
import dao.implementacionList.HistoriaDeProductoDAOList;
import dao.implementacionNio.HistoriaDeProductoDAONio;
import model.HistoriaDeProducto;

import java.util.List;

public class HistoriaDeProductoBsn {

    //private HistoriaDeProductoDAO historiaDeProductoDAO = new HistoriaDeProductoDAOList();
    private HistoriaDeProductoDAO historiaDeProductoDAO = new HistoriaDeProductoDAONio();

    public void registrarHistoriaDeProducto(HistoriaDeProducto h) {
        historiaDeProductoDAO.registrarHistoriaDeProducto(h);
    }

    public List<HistoriaDeProducto> listarHistoriaDeProducto(String codigo) throws ObjetoNoExisteException {
        try{
            return historiaDeProductoDAO.listarHistoriaDeProducto(codigo);
        }catch (LlaveNoExisteException lde){
            throw new ObjetoNoExisteException(String.format("La historia para el producto con codigo %s , no existe.", codigo));
        }
    }

}
