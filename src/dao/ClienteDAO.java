package dao;

import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import javafx.scene.effect.PerspectiveTransform;
import model.usuario.Cliente;
import model.usuario.Persona;

import java.util.List;
import java.util.Optional;

public interface ClienteDAO {

    List<Cliente> listarClientes();

    void registrarCliente(Cliente cliente) throws LlaveDuplicadaException;

    Optional<Cliente> consultarClientePorId(String id);

    void setPersonaLogueada(Persona persona);

    Persona getPersonaLogueaga();

}
