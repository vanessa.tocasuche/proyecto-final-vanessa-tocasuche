package dao;

import javafx.collections.ObservableList;
import model.Producto;
import model.ProductoAFacturar;

import java.util.List;
import java.util.Optional;

public interface ProductoAFacturarDAO {

    void agregarUnidadProductoAFacturar(String numeroDeFactura, Producto producto);
    void eliminarProductoAFacturar(String codigoProducto);
    List<ProductoAFacturar> productosEnLaFactura(String numeroDeFactura);
    Optional<ProductoAFacturar> optionalExisteNumDeFactura(String numeroDeFactura);


}
