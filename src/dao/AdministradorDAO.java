package dao;

import model.usuario.Administrador;
import model.usuario.Persona;

public interface AdministradorDAO {

    Administrador getAdministrador();

    void setPersonaLogueada(Persona persona);
    Persona getPersonaLogueada();
}
