package dao;

import model.Factura;
import model.Producto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface FacturaDAO {

    //metodos factura
    Factura getFactura();
    void setFactura(Factura f);
    boolean nuevaFactura(Factura h);
    void nuevaVenta();
    int totalAPagar(String numeroDeFactura);
    int pagar(String numeroDeFactura);


/*
    HashMap<String, Integer> productosAFacturar= new HashMap<>();   //codigo de producto, cantidad de unidades a vender
    List<Producto> productosAFacturarList = new ArrayList<>();      //Lista de productos que hacen parte de la factura

    //metodosList
    List<Producto> listarProductosAFacturar();
    void agregarProductoAFacturarList(Producto producto);

    //metodos HashMap
    void agregarProductoAHashMap(String codigo, int unidades);
    void eliminarProductoDeHashMap(String codigo );

    HashMap<String, Integer> getProductosAFacturar();
*/
}
