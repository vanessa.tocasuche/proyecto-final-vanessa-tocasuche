package dao;

import dao.exception.LlaveNoExisteException;
import model.HistoriaDeProducto;

import java.util.List;

public interface HistoriaDeProductoDAO {

    void registrarHistoriaDeProducto(HistoriaDeProducto h);
    List<HistoriaDeProducto> listarHistoriaDeProducto(String codigo) throws LlaveNoExisteException;

}
