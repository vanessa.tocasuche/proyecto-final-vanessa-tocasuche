package dao.implementacionNio;

import dao.ClienteDAO;
import dao.exception.LlaveDuplicadaException;
import model.usuario.Cliente;
import model.usuario.Persona;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class ClienteDAONio implements ClienteDAO {

    private final static String NOMBRE_ARCHIVO = "clientes";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public ClienteDAONio() {
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public List<Cliente> listarClientes() {
        List<Cliente> clientes = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> clientes.add(parseCliente2Object(clienteString)));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return clientes;
    }

    @Override
    public void registrarCliente(Cliente cliente) throws LlaveDuplicadaException {
        List<Cliente> clientes = listarClientes();
        Optional<Cliente> clienteOptional = clientes.stream()
                .filter(c -> c.getId().equals(cliente.getId()))
                .findFirst();
        if(clienteOptional.isPresent()) throw new LlaveDuplicadaException("El cliente ya existe.");

        String clienteString = parseCliente2String(cliente);
        byte[] datosRegistro = clienteString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public Optional<Cliente> consultarClientePorId(String id) {
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            Optional<String> clienteString = stream
                    .filter(cliente-> id.equals(cliente.split(",")[0]))
                    .findFirst();
            if(clienteString.isPresent()){
                return Optional.of(parseCliente2Object(clienteString.get()));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public void setPersonaLogueada(Persona persona) {
        Persona.setPersonaLogueada(persona);
    }

    @Override
    public Persona getPersonaLogueaga() {
        return Persona.getPersonaLogueada();
    }


    private Cliente parseCliente2Object(String clienteString) {
        String[] datosCliente = clienteString.split(FIELD_SEPARATOR);

        // todo: validar que el tamaño del arreglo sea de 3 elementos(id,contraseña,nombre)
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[2],
                datosCliente[1]);
        return cliente;
    }

    private String parseCliente2String(Cliente cliente) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(cliente.getId()).append(FIELD_SEPARATOR)
                .append(cliente.getContrasenia()).append(FIELD_SEPARATOR)
                .append(cliente.getNombre()).append(FIELD_SEPARATOR)
                .append(cliente.getDescuento()).append(FIELD_SEPARATOR)
                .append(cliente.getComprasAlmes()).append(RECORD_SEPARATOR);
        return stringBuffer.toString();
    }
}
