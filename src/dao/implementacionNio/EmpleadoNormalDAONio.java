package dao.implementacionNio;

import dao.EmpleadoNormalDAO;
import dao.exception.ClavesCredencialesIncorrectasException;
import dao.exception.DescuentoInvalidoDaoException;
import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import model.Producto;
import model.usuario.Cliente;
import model.usuario.Empleado;
import model.usuario.EmpleadoNormal;
import model.usuario.Persona;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class EmpleadoNormalDAONio implements EmpleadoNormalDAO {

    private final static String NOMBRE_ARCHIVO = "empleados";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public EmpleadoNormalDAONio() {
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public List<EmpleadoNormal> listarEmpleadosN() {
        List<EmpleadoNormal> empleados = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(empleadoString -> empleados.add(parseEmpleado2Object(empleadoString)));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return empleados;
    }

    private EmpleadoNormal parseEmpleado2Object(String s) {
        String[] datosEmpleado = s.split(FIELD_SEPARATOR);

        // todo: validar que el tamaño del arreglo sea de 3 elementos(id,contraseña,nombre)
        EmpleadoNormal empleado = new EmpleadoNormal(datosEmpleado[0], datosEmpleado[2],
                datosEmpleado[1],  datosEmpleado[3].charAt(0) , Integer.parseInt(datosEmpleado[4]) );
        return empleado;

    }

    @Override
    public void registrarEmpleadoN(EmpleadoNormal empleadoNormal) throws LlaveDuplicadaException, EdadIncorrectaException {
        List<EmpleadoNormal> empleados = listarEmpleadosN();
        Optional<EmpleadoNormal> empleadoOptional = empleados.stream()
                .filter(c -> c.getId().equals(empleadoNormal.getId()))
                .findFirst();
        if(empleadoOptional.isPresent()) throw new LlaveDuplicadaException("El empleado ya existe.");
        if( empleadoNormal.getEdad() < 18 ) throw new EdadIncorrectaException(String.format("La edad ingresada es incorrecta"));


        String empleadoString = parseEmpleado2String(empleadoNormal);
        byte[] datosRegistro = empleadoString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseEmpleado2String(EmpleadoNormal empleado) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(empleado.getId()).append(FIELD_SEPARATOR)
                .append(empleado.getContrasenia()).append(FIELD_SEPARATOR)
                .append(empleado.getNombre()).append(FIELD_SEPARATOR)
                .append(empleado.getGenero()).append(FIELD_SEPARATOR)
                .append(empleado.getEdad()).append(RECORD_SEPARATOR);
        return stringBuffer.toString();
    }


    @Override
    public void eliminarEmpleado(String id, String contrasenia) throws ClavesCredencialesIncorrectasException {
        List<EmpleadoNormal> empleados = listarEmpleadosN();
        if( !empleados.stream().anyMatch(e -> e.getId().equals(id) && e.getContrasenia().equals(contrasenia))) {
            throw new ClavesCredencialesIncorrectasException(String.format("Clave incorrecta"));
        }
        try {
            empleados.removeIf(e -> e.getId().equals(id) && e.getContrasenia().equals(contrasenia));
            sobreescribirArchivo(empleados);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<EmpleadoNormal> consultarEmpleadoPorId(String id) {
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            Optional<String> empleadoString = stream
                    .filter(e-> id.equals(e.split(",")[0]))
                    .findFirst();
            if(empleadoString.isPresent()){
                return Optional.of(parseEmpleado2Object(empleadoString.get()));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }



    @Override
    public void setPersonaLogueada(Persona persona) {
        Persona.setPersonaLogueada(persona);
    }

    @Override
    public Persona getPersonaLogueaga() {
        return Persona.getPersonaLogueada();
    }

    private void sobreescribirArchivo(List<EmpleadoNormal> empleados) {
        //elimina archivo
        if(Files.exists(ARCHIVO)){
            try {
                Files.delete(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
        //crea archivo
        try {
            Files.createFile(ARCHIVO);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        //escribe en el archivo
        for (EmpleadoNormal e : empleados) {
            try {
                registrarEmpleadoN(e);
            } catch (LlaveDuplicadaException lde) {
                lde.printStackTrace();
                System.out.println("error sobreecribiendo");
            } catch (EdadIncorrectaException eie) {
                eie.printStackTrace();
            }
        }
    }
}
