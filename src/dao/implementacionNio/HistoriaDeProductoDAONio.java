package dao.implementacionNio;

import dao.HistoriaDeProductoDAO;
import model.HistoriaDeProducto;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class HistoriaDeProductoDAONio implements HistoriaDeProductoDAO {

    private final static String NOMBRE_ARCHIVO = "historiaDeProductos";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public HistoriaDeProductoDAONio() {
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarHistoriaDeProducto(HistoriaDeProducto h) {
        List<HistoriaDeProducto> historias = listarHistoriaDeProducto(h.getCodigo());
        Optional<HistoriaDeProducto> hOptional = historias.stream()
                .filter(p -> p.getCodigo().equals(h.getCodigo()))
                .findFirst();

        String productoString = parseHistoria2String(h);
        byte[] datosRegistro = productoString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<HistoriaDeProducto> listarHistoriaDeProducto(String codigo) {

        List<HistoriaDeProducto> historiaString = new ArrayList<>();
        List<HistoriaDeProducto> historiaDeProducto = new ArrayList<>();
        try (Stream<String> linesString = Files.lines(ARCHIVO)) {
            linesString.forEach( string -> historiaString.add( parseHistoria2Object(string) ));
            historiaString.stream().filter(h -> h.getCodigo().equals(codigo)).forEach(historiaDeProducto::add);

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return historiaDeProducto;
    }

    private String parseHistoria2String(HistoriaDeProducto h) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(h.getCodigo()).append(FIELD_SEPARATOR)
                .append(h.getUnidadesAgregadas()).append(FIELD_SEPARATOR)
                .append(h.getUnidadesSacadas()).append(FIELD_SEPARATOR)
                .append(h.getUnidadesTotalesEnLocal()).append(FIELD_SEPARATOR)
                .append(h.getPrecioDeCompraDeUnidad()).append(FIELD_SEPARATOR)
                .append(h.getPrecioDeVenta()).append(RECORD_SEPARATOR);
        return stringBuffer.toString();
    }

    private HistoriaDeProducto parseHistoria2Object(String s) {
        String[] datosHistoriaProducto = s.split(FIELD_SEPARATOR);

        //
        HistoriaDeProducto historia = new HistoriaDeProducto(datosHistoriaProducto[0], Integer.parseInt(datosHistoriaProducto[1]),
                Integer.parseInt(datosHistoriaProducto[2]), Integer.parseInt(datosHistoriaProducto[3]),
                Integer.parseInt(datosHistoriaProducto[4]), Integer.parseInt(datosHistoriaProducto[5]));
        return historia;
    }
}
