package dao.implementacionNio;

import dao.ProductoDAO;
import dao.exception.DescuentoInvalidoDaoException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.NoSePuedeDescontarException;
import model.Producto;
import model.usuario.Cliente;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;


public class ProductoDAONio implements ProductoDAO {

    private final static String NOMBRE_ARCHIVO = "productos";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public ProductoDAONio() {
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public List<Producto> listarProductos() {
        List<Producto> productos = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(productosString -> productos.add( parseProducto2Object(productosString)) );
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return productos;
    }

    @Override
    public void registrarProducto(Producto producto) throws LlaveDuplicadaException, DescuentoInvalidoDaoException {
        List<Producto> productos = listarProductos();
        Optional<Producto> productoOptional = productos.stream()
                .filter(p -> p.getCodigo().equals(producto.getCodigo()))
                .findFirst();
        if(productoOptional.isPresent()) throw new LlaveDuplicadaException("El producto ya existe.");
        if(producto.getDescuento()>100) throw new DescuentoInvalidoDaoException( String.valueOf(producto.getDescuento()) );

        String productoString = parseProducto2String(producto);
        byte[] datosRegistro = productoString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void descontarProducto(String codigo) throws NoSePuedeDescontarException {
        List<Producto> productos = listarProductos();
        Optional<Producto> productoOptional = productos.stream()
                .filter(p -> ( p.getCodigo().equals(codigo) && p.getCantidadTotal()>0 ))
                .findFirst();
        if (!productoOptional.isPresent()){
            throw new NoSePuedeDescontarException(String.format("El producto no cuenta con unidades disponibles."));
        }else{
            productoOptional = productos.stream()
                    .filter(p -> ( p.getCodigo().equals(codigo)))
                    .findFirst();
            if(productoOptional.isPresent()) {
                int c = productoOptional.get().getCantidadTotal();
                productoOptional.get().setCantidadTotal(c - 1);
            }
            sobreescribirArchivo( productos );
        }
    }

    @Override
    public void descontarProductos(String codigo, int i) throws NoSePuedeDescontarException {
        List<Producto> productos= listarProductos();
        Optional<Producto> productoOptional = productos.stream()
                .filter(p -> ( p.getCodigo().equals(codigo) && p.getCantidadTotal()>=i ))
                .findFirst();
        if (!productoOptional.isPresent()){
            throw new NoSePuedeDescontarException(String.format("El producto no cuenta con unidades disponibles."));
        }else {
            productoOptional = productos.stream()
                    .filter(p -> (p.getCodigo().equals(codigo)))
                    .findFirst();
            if (productoOptional.isPresent()) {
                int c = productoOptional.get().getCantidadTotal();
                productoOptional.get().setCantidadTotal(c - i);
            }
            sobreescribirArchivo( productos );
        }
    }

    @Override
    public void modificarProducto(Producto producto) {
        List<Producto> productos = listarProductos();
        try {
            productos.removeIf(p -> p.getCodigo().equals(producto.getCodigo()));
            productos.add(producto);
            sobreescribirArchivo(productos);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminarProducto(String codigo) {
        List<Producto> productos = listarProductos();
        try {
            productos.removeIf(p -> p.getCodigo().equals(codigo));
            sobreescribirArchivo(productos);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Producto> consultarProductoPorCodigo(String codigo) {
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            Optional<String> pString = stream
                    .filter(p-> codigo.equals(p.split(",")[0]))
                    .findFirst();
            if(pString.isPresent()){
                return Optional.of(parseProducto2Object(pString.get()));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Producto> listarProductosConDescuento() {
        List<Producto> productos = new ArrayList<>();
        List<Producto> productosConDescuento = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(productosString -> productos.add( parseProducto2Object(productosString)) );
            productos.stream().filter( p -> p.getDescuento()>0).forEach(h -> productosConDescuento.add(h));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return productosConDescuento;
    }

    private Producto parseProducto2Object(String s) {
        String[] datosProducto = s.split(FIELD_SEPARATOR);

        //
        Producto producto = new Producto(datosProducto[0], datosProducto[1],
                Integer.parseInt(datosProducto[2]), Integer.parseInt(datosProducto[3]),
                Double.parseDouble(datosProducto[4]) );
        return producto;
    }

    private String parseProducto2String(Producto producto) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(producto.getCodigo()).append(FIELD_SEPARATOR)
                .append(producto.getNombre()).append(FIELD_SEPARATOR)
                .append(producto.getCantidadTotal()).append(FIELD_SEPARATOR)
                .append(producto.getPrecio()).append(FIELD_SEPARATOR)
                .append(producto.getDescuento()).append(RECORD_SEPARATOR);
        return stringBuffer.toString();
    }

    private void sobreescribirArchivo(List<Producto> productos) {
        //elimina archivo
        if(Files.exists(ARCHIVO)){
            try {
                Files.delete(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
        //crea archivo
        try {
            Files.createFile(ARCHIVO);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        //escribe en el archivo
        for (Producto p : productos) {
            try {
                registrarProducto(p);
            } catch (LlaveDuplicadaException lde) {
                lde.printStackTrace();
                System.out.println("error sobreecribiendo");
            } catch (DescuentoInvalidoDaoException die) {
                die.printStackTrace();
            }
        }
    }
}
