package dao.implementacionNio;

import dao.ProductoAFacturarDAO;
import model.Producto;
import model.ProductoAFacturar;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductoAFacturarDAONio implements ProductoAFacturarDAO{

    @Override
    public void agregarUnidadProductoAFacturar(String numeroDeFactura, Producto producto) {
        //si existe el ppoducto en la factura
        if ( optionalExisteProductoEnFactura(numeroDeFactura,  producto).isPresent()) {
            aumentaUnidadDeProductoEnfactura(numeroDeFactura, producto);

        }else {
            ProductoAFacturar productoAFacturar= new ProductoAFacturar(numeroDeFactura, producto,1);
        }
    }

    private void aumentaUnidadDeProductoEnfactura(String numeroDeFactura, Producto producto) {
        int cantidadAnterior= ProductoAFacturar.getProductosAFacturar()
                .stream()
                .filter(n -> (n.getNumeroDeFactura().equals(numeroDeFactura) && n.getProducto().getCodigo().equals(producto.getCodigo())))
                .findFirst()
                .get()
                .getCantidad();
        List<ProductoAFacturar> productosAFacturarEnLista= ProductoAFacturar.getProductosAFacturar();
        productosAFacturarEnLista.removeIf(n -> (n.getNumeroDeFactura().equals(numeroDeFactura) && n.getProducto().getCodigo().equals(producto.getCodigo())));
        ProductoAFacturar p= new ProductoAFacturar(numeroDeFactura,producto,cantidadAnterior+1);
    }

    @Override
    public void eliminarProductoAFacturar(String codigoProducto) {

    }

    @Override
    public List<ProductoAFacturar> productosEnLaFactura(String numeroDeFactura) {
        List<ProductoAFacturar> bd = ProductoAFacturar.getProductosAFacturar();
        return bd.stream()
                .filter( p -> p.getNumeroDeFactura().equals(numeroDeFactura))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ProductoAFacturar> optionalExisteNumDeFactura(String numeroDeFactura) {
        return ProductoAFacturar.getProductosAFacturar().stream()
                .filter(P -> P.getNumeroDeFactura().equals(numeroDeFactura))
                .findFirst();
    }
    public Optional<ProductoAFacturar> optionalExisteProductoEnFactura(String numeroDeFactura, Producto prod) {
        return ProductoAFacturar.getProductosAFacturar().stream()
                .filter(P -> (P.getNumeroDeFactura().equals(numeroDeFactura) && P.getProducto().getCodigo().equals(prod.getCodigo()) ))
                .findFirst();
    }


}
