package dao.implementacionNio;

import dao.FacturaDAO;
import dao.ProductoAFacturarDAO;
import model.Factura;
import model.ProductoAFacturar;
import model.usuario.Empleado;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class FacturaDAONio implements FacturaDAO {

    private final static String NOMBRE_ARCHIVO = "facturas";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = ",";
    private final static String RECORD_SEPARATOR = System.lineSeparator();


    /*private final HashMap<String, Integer> productosAFacturar= new HashMap<>();
    private final List<Producto> productosAFacturarList = new ArrayList<>();*/
    private Factura factura;
    private ProductoAFacturarDAO productosenFactura;

    public FacturaDAONio(){
        if(!Files.exists(ARCHIVO)){
            try {
                Files.createFile(ARCHIVO);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public boolean nuevaFactura(Factura f) {

        //poner numero de factura en txt
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(f.getNumeroDeFactura()).append(RECORD_SEPARATOR);
        String s = stringBuffer.toString();
        byte[] datosRegistro = s.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
            return generarArchivoDeFactura();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return false;
    }


    @Override
    public void nuevaVenta() {
        ArrayList<String> facturas = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(facturas::add);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        int t= facturas.size();
        try{
            factura = new Factura(t+1, LocalDate.now(), Empleado.getPersonaLogueada().getNombre());
        }catch (java.lang.NullPointerException e){
            System.out.println("Usuario no logueado");
        }
    }

    @Override
    public int totalAPagar(String numeroDeFactura) {
        List<ProductoAFacturar> productoDeFactura=  ProductoAFacturar.getProductosAFacturar().stream()
                .filter(N-> N.getNumeroDeFactura().equals(numeroDeFactura))
                .collect(Collectors.toList());
        int pagar= productoDeFactura.stream()
                .mapToInt(p -> (int) (p.getCantidad() * ( (p.getProducto().getPrecio())*(1 - p.getProducto().getDescuento() /100 ))))
                .sum();
        return pagar;
    }

    @Override
    public int pagar(String numeroDeFactura) {
        List<ProductoAFacturar> productoDeFactura=  ProductoAFacturar.getProductosAFacturar().stream()
                .filter(N-> N.getNumeroDeFactura().equals(numeroDeFactura))
                .collect(Collectors.toList());
        int pagar= productoDeFactura.stream()
                .mapToInt(p -> p.getCantidad() * p.getProducto().getPrecio())
                .sum();
        return pagar;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }


    private boolean generarArchivoDeFactura() {

        //nombre de archivo donde queda la factura
        final String NOMBRE_ARCHIVO_FACTURA = String.format("facturaN"+factura.getNumeroDeFactura());
        final Path ARCHIVO_FACTURA = Paths.get(NOMBRE_ARCHIVO_FACTURA);

        //crea el archivo si no existe
        if(!Files.exists(ARCHIVO_FACTURA)){
            try {
                Files.createFile(ARCHIVO_FACTURA);
            }catch (IOException ioe){
                ioe.printStackTrace();
            }
        }

        productosenFactura= new ProductoAFacturarDAONio();
        List<ProductoAFacturar> listaDeProdcutos = productosenFactura.productosEnLaFactura(String.valueOf( factura.getNumeroDeFactura() ));
        List<String> pp= new ArrayList<>();
        listaDeProdcutos.forEach(p ->  pp.add( String.format(
                p.getProducto().getCodigo() + "     " +
                        p.getProducto().getNombre() + "     " +
                        p.getCantidad() + "     " +
                        p.getProducto().getPrecio() + "     "+
                        p.getProducto().getPrecio() *  p.getCantidad() )));

        pp.stream().forEach(p-> System.out.println(p));

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Numero de factura: ").append(factura.getNumeroDeFactura()).append(RECORD_SEPARATOR)
                .append("Fecha: ").append(factura.getFechaDeVenta()).append(RECORD_SEPARATOR)
                .append("Atendido por: ").append(factura.getNombreEmpleado()).append(RECORD_SEPARATOR)
                .append("ID del cliente: ").append(factura.getIdDeCliente()).append(RECORD_SEPARATOR)
                .append("Atendido por: ").append(factura.getNombreEmpleado()).append(RECORD_SEPARATOR)
                .append(RECORD_SEPARATOR)
                .append("PRODUCTOS").append(RECORD_SEPARATOR)
                .append( pp.toString() + RECORD_SEPARATOR )
                .append(RECORD_SEPARATOR)
                .append(RECORD_SEPARATOR)
                .append("Pagar: ").append(pagar(String.valueOf( factura.getNumeroDeFactura()))).append(RECORD_SEPARATOR)
                .append("Con descuentos, total a pagar: ").append(totalAPagar(String.valueOf( factura.getNumeroDeFactura())))
                .append(RECORD_SEPARATOR);

        //escribe en el archivo
        String s = stringBuffer.toString();
        byte[] datosRegistro = s.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO_FACTURA, APPEND)) {
            fileChannel.write(byteBuffer);
            return true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return false;
    }

}
