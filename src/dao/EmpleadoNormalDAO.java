package dao;

import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.ClavesCredencialesIncorrectasException;
import model.usuario.EmpleadoNormal;
import model.usuario.Persona;

import java.util.List;
import java.util.Optional;

public interface EmpleadoNormalDAO {
    List<EmpleadoNormal> listarEmpleadosN();
    void registrarEmpleadoN(EmpleadoNormal empleadoNormal) throws LlaveDuplicadaException, EdadIncorrectaException;
    void eliminarEmpleado(String id, String contrasenia) throws ClavesCredencialesIncorrectasException;

    Optional<EmpleadoNormal> consultarEmpleadoPorId(String id);

    void setPersonaLogueada(Persona persona);
    Persona getPersonaLogueaga();
}
