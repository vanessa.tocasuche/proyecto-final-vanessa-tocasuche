package dao.implementacionList;

import dao.HistoriaDeProductoDAO;
import dao.exception.LlaveNoExisteException;
import model.HistoriaDeProducto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HistoriaDeProductoDAOList implements HistoriaDeProductoDAO {

    private static List<HistoriaDeProducto> listaDeHistoriaDeProductos = new ArrayList<>();

    @Override
    public void registrarHistoriaDeProducto(HistoriaDeProducto h)  {
        listaDeHistoriaDeProductos.add(h);
    }

    @Override
    public List<HistoriaDeProducto> listarHistoriaDeProducto(String codigo) throws LlaveNoExisteException {
        List<HistoriaDeProducto> lista= listaDeHistoriaDeProductos.stream()
                .filter(p -> p.getCodigo().equals(codigo))
                .collect(Collectors.toList());
        if(lista.isEmpty()){
            throw new LlaveNoExisteException(codigo);
        }
        return lista;
    }
}
