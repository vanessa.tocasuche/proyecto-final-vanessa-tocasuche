package dao.implementacionList;

import dao.EmpleadoNormalDAO;
import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.ClavesCredencialesIncorrectasException;
import model.usuario.Empleado;
import model.usuario.EmpleadoNormal;
import model.usuario.Persona;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmpleadoNormalDAOList implements EmpleadoNormalDAO {
    private static List<EmpleadoNormal> empleadosN= new ArrayList();


    @Override
    public List<EmpleadoNormal> listarEmpleadosN() {
        return new ArrayList<>(empleadosN);
    }

    @Override
    public void registrarEmpleadoN(EmpleadoNormal empleadoNormal) throws LlaveDuplicadaException, EdadIncorrectaException {
        Optional<EmpleadoNormal> empleadoNormalOptional = consultarEmpleadoPorId(empleadoNormal.getId());
        if(empleadoNormalOptional.isPresent()){
            throw new LlaveDuplicadaException(String.format( "El empleado ya existe." ));
        }
        if( empleadoNormal.getEdad() < 18 ){
            throw new EdadIncorrectaException(String.format("La edad ingresada es incorrecta"));
        }
        empleadosN.add(empleadoNormal);
    }

    @Override
    public void eliminarEmpleado(String id, String contrasenia) throws ClavesCredencialesIncorrectasException {
        Optional empleadoOpcional = empleadosN.stream().
                filter(p -> (p.getId().equals(id) && p.getContrasenia().equals(contrasenia) ))
                .findFirst();
        if(empleadoOpcional.isPresent()){
            empleadosN.removeIf(p -> p.getId().equals(id) );
        }else{
            throw new ClavesCredencialesIncorrectasException(String.format( "Credenciales incorrectas." ));
        }
    }

    @Override
    public Optional<EmpleadoNormal> consultarEmpleadoPorId(String id) {
        return empleadosN.stream().
                filter(em -> em.getId().equals(id)).
                findFirst();
    }
    @Override
    public void setPersonaLogueada(Persona persona) {
        Persona.setPersonaLogueada(persona);
    }
    @Override
    public Persona getPersonaLogueaga() {
        return Persona.getPersonaLogueada();
    }
}
