package dao.implementacionList;

import dao.ProductoDAO;
import dao.exception.DescuentoInvalidoDaoException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.NoSePuedeDescontarException;
import model.Producto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductoDAOList implements ProductoDAO {

    private static List<Producto> productos = new ArrayList<>();

    @Override
    public List<Producto> listarProductos() {
        return new ArrayList<>(productos);
    }

    @Override
    public void registrarProducto(Producto producto) throws LlaveDuplicadaException, DescuentoInvalidoDaoException {
        Optional<Producto> productoOptional = consultarProductoPorCodigo(producto.getCodigo());
        if(productoOptional.isPresent()){
            throw new LlaveDuplicadaException(String.format("El producto con codigo: %s, ya existe ",producto.getCodigo()));
        }
        if(producto.getDescuento()>100) throw new DescuentoInvalidoDaoException( String.valueOf(producto.getDescuento()) );
        productos.add(producto);
    }

    @Override
    public void descontarProducto(String codigo) throws NoSePuedeDescontarException {
        Optional<Producto> productoOptional = productos.stream()
                .filter(p -> ( p.getCodigo().equals(codigo) && p.getCantidadTotal()>0 ))
                .findFirst();
        if (!productoOptional.isPresent()){
            throw new NoSePuedeDescontarException(String.format("El producto no cuenta con unidades disponibles."));
        }else{
            productoOptional = productos.stream()
                    .filter(p -> ( p.getCodigo().equals(codigo)))
                    .findFirst();
            if(productoOptional.isPresent()){
                int c = productoOptional.get().getCantidadTotal();
                productoOptional.get().setCantidadTotal(c-1);
            }
        }

    }
    @Override
    public void descontarProductos(String codigo, int i) throws NoSePuedeDescontarException {
        Optional<Producto> productoOptional = productos.stream()
                .filter(p -> ( p.getCodigo().equals(codigo) && p.getCantidadTotal()>=i ))
                .findFirst();
        if (!productoOptional.isPresent()){
            throw new NoSePuedeDescontarException(String.format("El producto no cuenta con unidades disponibles."));
        }else{
            productoOptional = productos.stream()
                    .filter(p -> ( p.getCodigo().equals(codigo)))
                    .findFirst();
            if(productoOptional.isPresent()){
                int c = productoOptional.get().getCantidadTotal();
                productoOptional.get().setCantidadTotal(c-i);
            }
        }
    }

    @Override
    public void modificarProducto(Producto producto) {
        try {
            productos.removeIf(p -> p.getCodigo().equals(producto.getCodigo()));
            productos.add(producto);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void eliminarProducto(String codigo) {
        try {
            productos.removeIf(p -> p.getCodigo().equals(codigo));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Optional<Producto> consultarProductoPorCodigo(String codigo) {
        return productos.stream()
                .filter(c -> c.getCodigo().equals(codigo))
                .findFirst();
    }

    @Override
    public List<Producto> listarProductosConDescuento() {
        //implementar
        return new ArrayList<>();
    }
}
