package dao.implementacionList;

import dao.AdministradorDAO;
import model.usuario.Administrador;
import model.usuario.Persona;

public class AdministradorDAOList implements AdministradorDAO {
    private static Administrador administrador= new Administrador();

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setPersonaLogueada(Persona persona){
        Persona.setPersonaLogueada(persona);
    }
    public Persona getPersonaLogueada(){
        return Persona.getPersonaLogueada();
    }

}
