package dao.implementacionList;

import com.sun.org.apache.bcel.internal.generic.ATHROW;
import dao.ClienteDAO;
import dao.exception.EdadIncorrectaException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.LlaveNoExisteException;
import model.usuario.Cliente;
import model.usuario.Persona;

import java.lang.invoke.LambdaConversionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClienteDAOList implements ClienteDAO {
    private static List<Cliente> clientes= new ArrayList();

    @Override
    public List<Cliente> listarClientes() {
        return new ArrayList<>(clientes);
    }

    @Override
    public void registrarCliente(Cliente cliente) throws LlaveDuplicadaException {
        Optional<Cliente> clienteOptional= consultarClientePorId(cliente.getId());
        if(clienteOptional.isPresent()){
            throw new LlaveDuplicadaException(String.format("El cliente ya existe."));
        }
        clientes.add(cliente);
    }

    @Override
    public Optional<Cliente> consultarClientePorId(String id) {
        Optional<Cliente> cliente= clientes.stream()
                .filter(c -> c.getId().equals(id))
                .findFirst();
        if (cliente.isPresent()) {
            return clientes.stream()
                    .filter(c -> c.getId().equals(id))
                    .findFirst();
        }

        return Optional.empty();//******************
    }

    @Override
    public void setPersonaLogueada(Persona persona) {
        Persona.setPersonaLogueada(persona);
    }

    @Override
    public Persona getPersonaLogueaga() {
        return Persona.getPersonaLogueada();
    }
}
