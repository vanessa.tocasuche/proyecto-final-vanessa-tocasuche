package dao;

import dao.exception.DescuentoInvalidoDaoException;
import dao.exception.LlaveDuplicadaException;
import dao.exception.NoSePuedeDescontarException;
import model.Producto;

import java.util.List;
import java.util.Optional;

public interface ProductoDAO {
    List<Producto> listarProductos();

    void registrarProducto(Producto producto) throws LlaveDuplicadaException, DescuentoInvalidoDaoException;
    void descontarProducto(String codigo) throws NoSePuedeDescontarException;
    void descontarProductos(String codigo, int i) throws NoSePuedeDescontarException;
    void modificarProducto(Producto producto);
    void eliminarProducto(String codigo);

    Optional<Producto> consultarProductoPorCodigo(String codigo);

    List<Producto> listarProductosConDescuento();
}
