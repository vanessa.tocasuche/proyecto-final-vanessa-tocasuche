package dao.exception;

public class NoSePuedeDescontarException extends Exception {

    public NoSePuedeDescontarException(String message) {
        super(String.format("No hay suficiente numero de productos con la clave %s", message));
    }
}
