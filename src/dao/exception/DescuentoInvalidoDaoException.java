package dao.exception;

public class DescuentoInvalidoDaoException extends  Exception{
    public DescuentoInvalidoDaoException(String message) {
        super(String.format("La entrada %s de descuento es invalida.", message));
    }
}
