package dao.exception;

public class ClavesCredencialesIncorrectasException extends Exception{

    public ClavesCredencialesIncorrectasException(String llave) {
        super(String.format("credenciales incorrectas"));
    }
}
