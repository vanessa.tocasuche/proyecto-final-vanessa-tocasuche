package dao.exception;

public class LlaveNoExisteException extends Exception{
    public LlaveNoExisteException(String codigo) {
        super(String.format("La llave %s no existe", codigo));
    }
}
