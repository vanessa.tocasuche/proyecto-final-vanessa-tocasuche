package dao.exception;

public class EdadIncorrectaException extends Exception{
    public EdadIncorrectaException(String edad) {
        super(String.format("La entrada %s ingresada es incorrecta.", edad));
    }
}
