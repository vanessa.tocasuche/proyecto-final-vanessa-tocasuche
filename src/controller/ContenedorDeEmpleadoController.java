package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ContenedorDeEmpleadoController {

    @FXML
    private BorderPane contenedorEmpleado;

    public void btnNuevaVenta_action(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/local/nueva-venta.fxml"));
            this.contenedorEmpleado.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnVerProductos_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(getClass().getResource("../view/local/local-comercial.fxml"));
            this.contenedorEmpleado.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnSalir_action(ActionEvent actionEvent) {
        System.exit(0);
    }
}
