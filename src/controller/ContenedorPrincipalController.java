package controller;

import bsn.AdministradorBsn;
import bsn.ClienteBsn;
import bsn.EmpleadoNormalBsn;
import controller.usuario.CredencialesUsuarioController;
import controller.usuario.RegistrarClienteController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.usuario.Cliente;
import model.usuario.Persona;

import java.io.IOException;
import java.util.Optional;


public class ContenedorPrincipalController {

    /*public static boolean logueoCorrectoCliente=false;
    public static boolean logueoCorrectoEmpleadoN=false;
    public static boolean logueoCorrectoAdministrador=false;
    public static boolean isLogueoCorrectoCliente() {
        return logueoCorrectoCliente;
    }
    public static void setLogueoCorrectoCliente(boolean logueoCorrectoCliente) {
        ContenedorPrincipalController.logueoCorrectoCliente = logueoCorrectoCliente;
    }
    public static boolean isLogueoCorrectoEmpleadoN() {
        return logueoCorrectoEmpleadoN;
    }
    public static void setLogueoCorrectoEmpleadoN(boolean logueoCorrectoEmpleadoN) {
        ContenedorPrincipalController.logueoCorrectoEmpleadoN = logueoCorrectoEmpleadoN;
    }
    public static boolean isLogueoCorrectoAdministrador() {
        return logueoCorrectoAdministrador;
    }
    public static void setLogueoCorrectoAdministrador(boolean logueoCorrectoAdministrador) {
        ContenedorPrincipalController.logueoCorrectoAdministrador = logueoCorrectoAdministrador;
    }*/

    @FXML
    private BorderPane contenedorPrincipal;
    private Stage primaryStage;

    private static ClienteBsn clienteBsn = new ClienteBsn();
    private static EmpleadoNormalBsn empleadoNormalBsn = new EmpleadoNormalBsn();
    private static AdministradorBsn administradorBsn = new AdministradorBsn();

    public void mnuSalir_action(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void mnuRegistrarCliente_action(){
        try {
            FXMLLoader loader =  new FXMLLoader(getClass().getResource("../view/usuario/registrar-cliente.fxml"));
            AnchorPane registrarCliente = loader.load();
            Scene scene = new Scene(registrarCliente);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            //popup

        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }
    /*public void mnuRegistroDeProducto_action(){
        try {
            AnchorPane registrarCliente = FXMLLoader
                    .load(getClass().getResource("../view/local/registro-de-productos.fxml"));
            this.contenedorPrincipal.setCenter(registrarCliente);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }*/

    public void mnuLoguearUsuario_action(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/usuario/credenciales-usuario.fxml"));
            AnchorPane credencialesUsuario = loader.load();
            CredencialesUsuarioController cuc= loader.getController();
            Scene scene = new Scene(credencialesUsuario);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            cuc.setStageCredenciales(stage);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void abrirContenedorDeCliente_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-cliente.fxml"));
            this.contenedorPrincipal.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    public void abrirContenedorDeEmpleado_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-empleado.fxml"));
            this.contenedorPrincipal.setCenter(localComercial);

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    public void abrirContenedorDeAdministrador_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-administrador.fxml"));
            this.primaryStage.setTitle("Administrador");
            this.contenedorPrincipal.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }


    public void mnuVerProductos_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(getClass().getResource("../view/local/local-comercial.fxml"));
            this.contenedorPrincipal.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void mnuAcercaDe_action(ActionEvent actionEvent) {
    }

    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }


/*
    public void loguear(){
        String idLogueado = Persona.getPersonaLogueada().getId();
        if( clienteBsn.consultarClientePorId(idLogueado).isPresent() ){
            abrirContenedorDeCliente_action();

        }else if(empleadoNormalBsn.consultarEmpleadoPorId(idLogueado).isPresent()){
            abrirContenedorDeEmpleado_action();

        }else if(administradorBsn.getAdministrador().getId().equals(idLogueado)){
            abrirContenedorDeAdministrador_action();
        }
    }*/

/*
    public void abrirContenedorDeCliente_action() {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-cliente.fxml"));
            contenedorPrincipal.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    public static void abrirContenedorDeEmpleado_action() {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-empleado.fxml"));
            contenedorPrincipal.setCenter(localComercial);

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    public static void abrirContenedorDeAdministrador_action() {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-administrador.fxml"));
            primaryStage.setTitle("Administrador");
            contenedorPrincipal.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }*/
}
