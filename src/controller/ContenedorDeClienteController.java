package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ContenedorDeClienteController {

    @FXML
    private BorderPane contenedorCliente;

    public void btnSalir_action(ActionEvent actionEvent) {
        System.exit(0);
    }

    public void btnMiCuenta_action(ActionEvent actionEvent) {
        /*try {
            AnchorPane anchorPane = FXMLLoader
                    .load(getClass().getResource("../usuario/ver-cuenta.fxml"));
            this.contenedorCliente.setCenter(anchorPane);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }*/
    }

    public void btnVerProductos_action(ActionEvent actionEvent) {
        try {
            AnchorPane localComercial = FXMLLoader
                    .load(getClass().getResource("../view/local/local-comercial.fxml"));
            this.contenedorCliente.setCenter(localComercial);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
