package controller;

import bsn.AdministradorBsn;
import dao.AdministradorDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.usuario.Persona;

import java.io.IOException;

public class ContenedorAdministradorController {
    @FXML
    private BorderPane contenedorAdministrador;



    public void btnNuevaVenta_action(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/local/nueva-venta.fxml"));
            this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnEliminarTrabajador_action(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/usuario/quitar-empleado.fxml"));
            this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnNuevoTrabajador_action(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/usuario/registrar-empleado.fxml"));
            this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }


    public void btnRegistrarNuevoProducto_action(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/local/registro-de-productos.fxml"));
            this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnModificarProductos_action(ActionEvent actionEvent) {
        try {
        AnchorPane contenedor = FXMLLoader
                .load(ContenedorPrincipalController.class.getResource("../view/local/modificar-productos.fxml"));
        this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void btnHistoriaDeProducto(ActionEvent actionEvent) {
        try {
            AnchorPane contenedor = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/local/historia-producto.fxml"));
            this.contenedorAdministrador.setCenter(contenedor);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

    public void btnSalir_action(ActionEvent actionEvent) {
        System.exit(0);
    }
}
