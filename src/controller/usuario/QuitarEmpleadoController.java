package controller.usuario;

import bsn.EmpleadoNormalBsn;
import bsn.exception.CredencialesIncorrectasException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.usuario.EmpleadoNormal;

import java.util.List;

public class QuitarEmpleadoController {

    @FXML
    private TextField txtId;
    @FXML
    private PasswordField txtContrasenia;
    @FXML
    private ComboBox<EmpleadoNormal> cmbEmpleados;

    private EmpleadoNormalBsn empleadoBSN= new EmpleadoNormalBsn();

    @FXML
    public void initialize() {
        List<EmpleadoNormal> empleados = empleadoBSN.listarEmpleadosN();
        ObservableList<EmpleadoNormal> empleadosObservables = FXCollections.observableList(empleados);
        this.cmbEmpleados.setItems(empleadosObservables);

    }

    public void cmbEmpleados_action(ActionEvent actionEvent) {

    }

    public void btnEliminarEmpleado_action(ActionEvent actionEvent) {
        EmpleadoNormal empleadoSeleccionado = cmbEmpleados.getValue();
        try{
            empleadoBSN.eliminarEmpleado(txtId.getText(),txtContrasenia.getText());
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Eliminar cliente");
            alert.setHeaderText("Proceso finalizado correctamente.");
            alert.setContentText("El usuario se elimino de manera correcta");
            alert.showAndWait();
            limpiarCampos();
        }catch (CredencialesIncorrectasException lde){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eliminar cliente");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText("Contraseña incorrecta");
            alert.showAndWait();
            limpiarCampos();
        }
    }
    private void limpiarCampos(){
        txtId.clear();
        txtContrasenia.clear();
        initialize();
    }

}
