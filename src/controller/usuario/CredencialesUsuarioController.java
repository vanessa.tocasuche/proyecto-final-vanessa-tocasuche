package controller.usuario;

import bsn.AdministradorBsn;
import bsn.ClienteBsn;
import bsn.EmpleadoNormalBsn;
import com.sun.javaws.Main;
import controller.ContenedorPrincipalController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.usuario.*;

import java.io.IOException;
import java.util.Optional;


public class CredencialesUsuarioController {
    @FXML
    private TextField txtId;
    @FXML
    private PasswordField txtContrasenia;
    @FXML
    private RadioButton rbtnCliente=new RadioButton();
    @FXML
    private RadioButton rbtnEmpleado=new RadioButton();;
    @FXML
    private RadioButton rbtnAdministrador=new RadioButton();
    private ToggleGroup grupoUsuario= new ToggleGroup();

    //Escenario
    private Stage stageCredenciales;

    //relaciones
    private ClienteBsn clienteBsn = new ClienteBsn();
    private EmpleadoNormalBsn empleadoNormalBsn = new EmpleadoNormalBsn();
    private AdministradorBsn administradorBsn = new AdministradorBsn();

    @FXML
    public void initialize() {
        rbtnCliente.setToggleGroup(grupoUsuario);
        rbtnEmpleado.setToggleGroup(grupoUsuario);
        rbtnAdministrador.setToggleGroup(grupoUsuario);
        rbtnCliente.setSelected(true);
    }

    public void btnIngresar_action(ActionEvent actionEvent) {
        String idIngresado = txtId.getText().trim();
        String contraseniaIngresada = txtContrasenia.getText().trim();


        boolean esValido = validarCampos(idIngresado, contraseniaIngresada);
        Alert alert;

        if (!esValido) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ingreso");
            alert.setHeaderText("Ingreso");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
        }

        if( rbtnCliente.isSelected() ){
            Optional<Cliente> clienteOptional= clienteBsn.listarClientes()
                    .stream()
                    .filter(c -> c.getId().equals(idIngresado))
                    .findFirst();
            if ( clienteOptional.isPresent() && clienteOptional.get().getContrasenia().equals(contraseniaIngresada) ) {

                clienteBsn.setClienteLogueado( clienteOptional.get() );
                logueoExitoso(true);
                abrirContenedorDeCliente();

            }else logueoExitoso(false);
        }

        else if (rbtnEmpleado.isSelected()){
            Optional<EmpleadoNormal> empleadoNormalOptional= empleadoNormalBsn.listarEmpleadosN()
                    .stream()
                    .filter(c -> c.getId().equals(idIngresado))
                    .findFirst();
            if ( empleadoNormalOptional.isPresent() && empleadoNormalOptional.get().getContrasenia().equals(contraseniaIngresada) ){

                empleadoNormalBsn.setEmpleadoLogueado( empleadoNormalOptional.get() );
                logueoExitoso(true);
                abrirContenedorDeEmpleado();

            }else logueoExitoso(false);
        }

        else if(rbtnAdministrador.isSelected()){
            if( administradorBsn.getAdministrador().getId().equals(idIngresado) && administradorBsn.getAdministrador().getContrasenia().equals(contraseniaIngresada) ){

                administradorBsn.setPersonaLogueada( administradorBsn.getAdministrador() );
                logueoExitoso(true);
                abrirContenedorDeAdministrador();

            }else logueoExitoso(false);
        }
    }

    //metodos

    private void logueoExitoso(boolean exito){
        Alert alert;
        if (exito){
            this.stageCredenciales.close();
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Credenciales Correctas.");
            alert.setHeaderText("Logueo éxitoso");
            alert.setContentText("Bienvenido. ");
            alert.showAndWait();

        }
        else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Contraseña incorrecta");
            alert.setContentText("Vuelva a ingresar los datos");
            alert.showAndWait();
        }
        limpiarCampos();

    }

    private boolean validarCampos(String... campos){
        for (String campo : campos) {
            if (campo == null || "".equals(campo)) {
                return false;
            }
        }
        return true;
    }

    public void limpiarCampos(){
        txtContrasenia.clear();
        txtId.clear();
    }

    public void abrirContenedorDeCliente() {

        try {
            AnchorPane anchorPane = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-cliente.fxml"));
            Scene scene = new Scene(anchorPane);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();


        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void abrirContenedorDeEmpleado() {
        try {
            AnchorPane anchorPane = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-empleado.fxml"));
            Scene scene = new Scene(anchorPane);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void abrirContenedorDeAdministrador() {
        try {
            AnchorPane anchorPane = FXMLLoader
                    .load(ContenedorPrincipalController.class.getResource("../view/contenedor-administrador.fxml"));
            Scene scene = new Scene(anchorPane);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }


    //getters and setters

    public void setStageCredenciales(Stage stageCredenciales) {
        this.stageCredenciales = stageCredenciales;
    }

}
