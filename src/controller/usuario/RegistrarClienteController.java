package controller.usuario;

import bsn.ClienteBsn;
import bsn.exception.ObjetoYaExisteException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import model.usuario.Cliente;


public class RegistrarClienteController {
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtNombreCompleto;
    @FXML
    private PasswordField txtContrasenia;

    private ClienteBsn clienteBsn = new ClienteBsn();


    @FXML
    public void initialize() {

        txtId.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 15) {
                return change;
            }
            return null;
        }));

    }

    public void btnRegistrarse_action() {
        String idIngresado = txtId.getText().trim();
        String nombreIngresado = txtNombreCompleto.getText().trim();
        String contraseniaIngresada = txtContrasenia.getText().trim();
        Alert alert;

        boolean esValido = validarCampos(idIngresado, nombreIngresado, contraseniaIngresada);

        if (!esValido) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Registro de cliente");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        Cliente cliente = new Cliente(idIngresado, nombreIngresado, contraseniaIngresada);

        try {
            clienteBsn.registrarCliente(cliente);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
        } catch (ObjetoYaExisteException e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            limpiarCampos();
        }

    }

    private boolean validarCampos(String... campos) {
        for (String campo : campos) {
            if (campo == null || "".equals(campo)) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtId.clear();
        txtNombreCompleto.clear();
        txtContrasenia.clear();
    }


}
