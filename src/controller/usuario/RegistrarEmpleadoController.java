package controller.usuario;

import bsn.EmpleadoNormalBsn;
import bsn.exception.ObjetoYaExisteException;
import dao.exception.EdadIncorrectaException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.usuario.EmpleadoNormal;


public class RegistrarEmpleadoController {
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtNombreCompleto;
    @FXML
    private PasswordField txtContrasenia;
    @FXML
    private TextField txtEdad;
    @FXML
    private ComboBox<String> cmbGenero;

    private EmpleadoNormalBsn empleadoNormalBsn = new EmpleadoNormalBsn();


    @FXML
    public void initialize() {
        ObservableList<String> listaGenero = FXCollections.observableArrayList("Femenino", "Masculino");
        cmbGenero.setItems(listaGenero);
        txtId.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 15) {
                return change;
            }
            return null;
        }));
        txtEdad.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 2) {
                return change;
            }
            return null;
        }));

    }

    public void btnRegistrarse_action() {
        String idIngresado = txtId.getText().trim();
        String nombreIngresado = txtNombreCompleto.getText().trim();
        String contraseniaIngresada = txtContrasenia.getText().trim();
        int edad = Integer.parseInt(txtEdad.getText().trim());
        char genero = cmbGenero.getValue().charAt(0);

        Alert alert;
        boolean esValido = validarCampos(idIngresado, nombreIngresado, contraseniaIngresada, edad, genero);

        if (!esValido) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Registro de cliente");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        EmpleadoNormal empleadoNormal = new EmpleadoNormal(idIngresado, nombreIngresado, contraseniaIngresada, genero, edad);

        try {
            empleadoNormalBsn.registrarEmpleadoN(empleadoNormal);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de empleado");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
        } catch (ObjetoYaExisteException lde) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de empleado");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(lde.getMessage());
            alert.showAndWait();
            limpiarCampos();
        } catch (EdadIncorrectaException lde){
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de empleado");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(lde.getMessage());
            alert.showAndWait();
        }

    }

    private boolean validarCampos(Object... campos) {
        for (Object campo : campos) {
            if (campo == null || "".equals(campo)) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtId.clear();
        txtNombreCompleto.clear();
        txtContrasenia.clear();
        txtEdad.clear();
    }


}
