package controller.local;

import bsn.ProductoBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Producto;

import java.util.List;

public class LocalComercialController {

    @FXML
    private RadioButton rbtnTodosLosProductos;
    @FXML
    private RadioButton rbtnProductosConDescuento;
    private ToggleGroup grupoRadioButton = new ToggleGroup();
    @FXML
    private TableView<Producto> tblProductos;
    @FXML
    private TableColumn<Producto, String> clmProducto;
    @FXML
    private TableColumn<Producto, String> clmCodigo;
    @FXML
    private TableColumn<Producto, String> clmPrecio;
    @FXML
    private TableColumn<Producto, String> clmDisponibilidad;
    @FXML
    private TableColumn<Producto, String> clmDescuento;

    ProductoBsn productoBsn = new ProductoBsn();

    @FXML
    private void initialize() {
        rbtnTodosLosProductos.setToggleGroup(grupoRadioButton);
        rbtnProductosConDescuento.setToggleGroup(grupoRadioButton);
        rbtnTodosLosProductos.setSelected(true);

        List<Producto> productos = productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        tblProductos.setItems(productosObservables);

        clmProducto.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecio())));
        clmDisponibilidad.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCantidadTotal())));
        clmDescuento.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getDescuento() )));
        tblProductos.setItems(productosObservables);

    }

    public void rbtnProductosConDescuento_action(ActionEvent actionEvent) {
        if(rbtnProductosConDescuento.isSelected()) {
            List<Producto> productos = productoBsn.listarProductosConDescuentos();
            ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
            tblProductos.setItems(productosObservables);

            clmProducto.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
            clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
            clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecio())));
            clmDisponibilidad.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCantidadTotal())));
            clmDescuento.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getDescuento() )));
            tblProductos.setItems(productosObservables);
        }
    }

    public void rbtnTodosLosProductos_action(ActionEvent actionEvent) {
        initialize();
    }
}