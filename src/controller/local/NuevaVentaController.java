package controller.local;


import bsn.ClienteBsn;
import bsn.FacturaBsn;
import bsn.ProductoAFacturarBsn;
import bsn.ProductoBsn;
import bsn.exception.ClienteNoExisteException;
import bsn.exception.NoHayProductosDisponiblesException;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Factura;
import model.Local;
import model.Producto;
import model.ProductoAFacturar;
import model.usuario.Cliente;
import model.usuario.Persona;

import javax.print.attribute.standard.NumberOfDocuments;
import java.time.LocalDate;
import java.util.List;
import java.util.Observable;
import java.util.Optional;


public class NuevaVentaController {
    @FXML
    private ComboBox<Producto> cmbProductos;
    @FXML
    private TextField txtNumeroDeFactura;
    @FXML
    private TextField txtFecha;
    @FXML
    private TextField txtNIT;
    @FXML
    private TextField txtVendedor;
    @FXML
    private TextField txtCliente;
    @FXML
    private Label txtDescuentoCliente;
    @FXML
    private TextField txtProducto;
    @FXML
    private Label lblPagar;
    @FXML
    private Label lblDescuento;
    @FXML
    private Label lblDescuentoClienteFinal;
    @FXML
    private Label lblTotalAPagar;
    @FXML
    private TableView<ProductoAFacturar> tblProductosAFacturar;
    @FXML
    private TableColumn<ProductoAFacturar, String> clmCodigo;
    @FXML
    private TableColumn<ProductoAFacturar, String> clmNombre;
    @FXML
    private TableColumn<ProductoAFacturar, String> clmPrecio;
    @FXML
    private TableColumn<ProductoAFacturar, String> clmUnidades;
    @FXML
    private TableColumn<ProductoAFacturar, Double> clmDescuento;
    @FXML
    private TableColumn<ProductoAFacturar, Double> clmTotal;

    private FacturaBsn facturaBsn = new FacturaBsn();
    private ClienteBsn clienteBsn =new ClienteBsn();
    private ProductoBsn productoBsn = new ProductoBsn();
    private ProductoAFacturarBsn productoAFacturarBsn=new ProductoAFacturarBsn();

    private int descuentoCliente = 0;


    private Factura factura;

    @FXML
    public void initialize(){
        facturaBsn.nuevaVenta();    //crea la nueva factura
        factura= facturaBsn.getFactura();

        txtVendedor.setText(facturaBsn.getFactura().getNombreEmpleado());
        txtNIT.setText(Local.getNit());
        txtNumeroDeFactura.setText(String.valueOf( factura.getNumeroDeFactura() ));
        txtFecha.setText(facturaBsn.getFactura().getFechaDeVenta().toString());
        txtVendedor.setEditable(false);
        txtNIT.setEditable(false);
        txtNumeroDeFactura.setEditable(false);
        txtVendedor.setEditable(false);
        txtFecha.setEditable(false);

        //Cargar productos en comboBox
        List<Producto> productos = this.productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        this.cmbProductos.setItems(productosObservables);

    }

    public void btnEliminarProducto_action(ActionEvent actionEvent) {

    }

    public void btnVerificarCliente_action(ActionEvent actionEvent) {
        try{
            Optional<Cliente> clienteOptional= clienteBsn.consultarClientePorId(txtCliente.getText());

            if (clienteOptional.isPresent()) {
                descuentoCliente =clienteOptional.get().getDescuento().intValue();
                txtDescuentoCliente.setText(String.format("Descuento: "+ descuentoCliente));
            }

        }catch (ClienteNoExisteException cne){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Cliente");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(String.valueOf(cne));
            alert.showAndWait();
        }
    }

    public void btnFacturar_action(ActionEvent actionEvent) {
        List<ProductoAFacturar> listaProductosEnFactura= productoAFacturarBsn.productosEnLaFactura(String.valueOf( factura.getNumeroDeFactura()));
        listaProductosEnFactura.stream().forEach(p -> {
            try {
                productoBsn.descontarProductos(p.getProducto().getCodigo(), p.getCantidad());
            } catch (NoHayProductosDisponiblesException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Producto");
                alert.setHeaderText("Ha ocurrido un error.");
                alert.setContentText(String.valueOf(e));
                alert.showAndWait();
            }
        });

        if(facturaBsn.nuevaFactura(factura)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Factura");
            alert.setHeaderText("Factura creada de manera correcta.");
            alert.setContentText("Puedes consultar la factura creada en la raiz del proyecto");
            alert.showAndWait();
        }


        limpiarCampos();
    }


    public void cmbProductos_action(ActionEvent actionEvent) {
        txtProducto.clear();
    }

    public void txtProducto_action(ActionEvent actionEvent) {
        //limpiar el combo box producto
    }

    public void btnAgregarProducto_action(ActionEvent actionEvent) {
        if(!txtProducto.getText().equals("")){
            Optional<Producto> optionalProducto= productoBsn.listarProductos()
                    .stream()
                    .filter(p-> txtProducto.getText().equals(p.getCodigo()))
                    .findFirst();
            optionalProducto.ifPresent(producto -> productoAFacturarBsn
                    .agregarUnidadProductoAFacturar( String.valueOf(factura.getNumeroDeFactura()) , producto) );
        }else {
            Optional<Producto> optionalProducto= productoBsn.listarProductos()
                    .stream()
                    .filter(p-> cmbProductos.getValue().getCodigo().equals(p.getCodigo()))
                    .findFirst();
            optionalProducto.ifPresent(producto -> productoAFacturarBsn
                    .agregarUnidadProductoAFacturar( String.valueOf(factura.getNumeroDeFactura()), producto) );

        }
        int pagar= facturaBsn.pagar(String.valueOf( facturaBsn.getFactura().getNumeroDeFactura()));
        int totalAPagarSinDescuentoCliente = facturaBsn.totalAPagar(String.valueOf( facturaBsn.getFactura().getNumeroDeFactura()));
        int totalAPagar = totalAPagarSinDescuentoCliente
                * ( 1 - totalAPagarSinDescuentoCliente * (descuentoCliente) / 100);
        lblPagar.setText( String.valueOf(pagar) );
        lblDescuento.setText( String.valueOf(pagar-totalAPagarSinDescuentoCliente ) );
        lblDescuentoClienteFinal.setText( String.valueOf(totalAPagarSinDescuentoCliente-totalAPagar) );
        lblTotalAPagar.setText( String.valueOf(totalAPagar) );
        ponerProductosEnTabla();

    }

    private void ponerProductosEnTabla() {
        List<ProductoAFacturar> productosEnLaFactura = productoAFacturarBsn.productosEnLaFactura(String.valueOf( factura.getNumeroDeFactura()));
        ObservableList<ProductoAFacturar> productoAFacturarObservable = FXCollections.observableList(productosEnLaFactura);

        clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProducto().getCodigo()));
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProducto().getNombre()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getProducto().getPrecio())));
        clmUnidades.setCellValueFactory(cellData -> new SimpleStringProperty( String.valueOf(cellData.getValue().getCantidad() )));
        clmDescuento.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getProducto().getDescuento()).asObject());
        clmTotal.setCellValueFactory(cellData -> new SimpleDoubleProperty
                ( (cellData.getValue().getProducto().getPrecio())
                        * (1 - cellData.getValue().getProducto().getDescuento()/100)
                        * cellData.getValue().getCantidad() )
                .asObject() );
        tblProductosAFacturar.setItems(productoAFacturarObservable);
    }

    private void limpiarCampos() {
        initialize();
        txtCliente.clear();
        lblDescuento.setText("");
        lblPagar.setText("0");
        lblDescuentoClienteFinal.setText("0");
        lblTotalAPagar.setText("0");
    }
}
