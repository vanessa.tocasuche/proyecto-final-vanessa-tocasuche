package controller.local;

import bsn.ProductoBsn;
import bsn.exception.DescuentoInvalidoException;
import bsn.exception.ObjetoYaExisteException;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.Producto;

import java.util.List;


public class RegistroDeProductosController {
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtCantidad;
    @FXML
    private TextField txtPrecio;
    @FXML
    private TableView<Producto> tblProductos;
    @FXML
    private TableColumn<Producto, String> clmNombre;
    @FXML
    private TableColumn<Producto, String> clmCodigo;
    @FXML
    private TableColumn<Producto, String> clmCantidad;
    @FXML
    private TableColumn<Producto, String> clmPrecio;
    @FXML
    private TableColumn<Producto, Double> clmDescuento;

    private ProductoBsn productoBsn= new ProductoBsn();

    @FXML
    public void initialize() {

        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
        clmCantidad.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCantidadTotal())));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecio())));
        clmDescuento.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getDescuento()).asObject());
        List<Producto> productos = this.productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        tblProductos.setItems(productosObservables);

        txtCantidad.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?")) {
                return change;
            }
            return null;
        }));
        txtPrecio.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?")) {
                return change;
            }
            return null;
        }));

    }

    public void btnRegistrar_action() {
        String nombreIngresado = txtNombre.getText().trim();
        String codigoIngresado = txtCodigo.getText().trim();
        String cantidadTmp = txtCantidad.getText().trim();
        String precioTmp = txtPrecio.getText().trim();
        int cantidadIngresada;
        int precioIngresado;

        if(cantidadTmp.equals("")) cantidadIngresada =0;
        else cantidadIngresada = Integer.parseInt(cantidadTmp);
        if (precioTmp.equals("")) precioIngresado=0;
        else precioIngresado = Integer.parseInt(precioTmp);

        boolean esValido = validarCampos(nombreIngresado, codigoIngresado);
        Alert alert;

        if (!esValido) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de producto");
            alert.setHeaderText("Registro de prodcuto");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }
        Producto producto = new Producto(codigoIngresado,nombreIngresado,cantidadIngresada,precioIngresado);

        try{
            productoBsn.registrarProducto(producto);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de prodcuto");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
            actualizarTabla();
        }catch (ObjetoYaExisteException e){
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de producto");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            limpiarCampos();
        } catch (DescuentoInvalidoException e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de producto");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
        List<Producto> productos = this.productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        tblProductos.setItems(productosObservables);

    }


    private boolean validarCampos(String... campos) {
        for (String campo : campos) {
            if (campo == null || "".equals(campo)) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtNombre.clear();
        txtPrecio.clear();
        txtCodigo.setText("");
        txtCantidad.clear();
    }

    private void actualizarTabla(){

    }


}
