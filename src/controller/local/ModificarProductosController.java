package controller.local;

import bsn.HistoriaDeProductoBsn;
import bsn.ProductoBsn;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.HistoriaDeProducto;
import model.Producto;

import java.util.List;

public class ModificarProductosController {
    @FXML
    private ComboBox<Producto> cmbCodigoNombre;
    @FXML
    private TextField txtCantidad;
    @FXML
    private TextField txtPrecio;
    @FXML
    private TextField txtDescuento;
    @FXML
    private RadioButton rbtnCambiarCantidad=new RadioButton();;
    @FXML
    private RadioButton rbtnAgregarCantidad=new RadioButton();
    private ToggleGroup grupoRButtonCantidad= new ToggleGroup();
    @FXML
    private Label lblCantidadActual;
    @FXML
    private Label lblPrecioActual;
    @FXML
    private Label lblDescuentoActual;
    @FXML
    private TableView<Producto> tblProductos;
    @FXML
    private TableColumn<Producto, String> clmNombre;
    @FXML
    private TableColumn<Producto, String> clmCodigo;
    @FXML
    private TableColumn<Producto, String> clmCantidad;
    @FXML
    private TableColumn<Producto, String> clmPrecio;
    @FXML
    private TableColumn<Producto, Double> clmDescuento;

    private ProductoBsn productoBsn= new ProductoBsn();
    private HistoriaDeProductoBsn historiaDeProductoBsn = new HistoriaDeProductoBsn();

    @FXML
    private void initialize(){
        List<Producto> productos = this.productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        this.cmbCodigoNombre.setItems(productosObservables);

        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
        clmCantidad.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getCantidadTotal())));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecio())));
        clmDescuento.setCellValueFactory(cellData -> new SimpleDoubleProperty(cellData.getValue().getDescuento()).asObject());
        tblProductos.setItems(productosObservables);
        
        txtCantidad.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][0-9]*)?")) {
                return change;
            }
            return null;
        }));
        txtPrecio.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][0-9]*)?")) {
                return change;
            }
            return null;
        }));
        txtDescuento.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([0-9][0-9]*)?") && change.getControlNewText().length() < 3) {
                return change;
            }
            return null;
        }));
        rbtnAgregarCantidad.setToggleGroup(grupoRButtonCantidad);
        rbtnCambiarCantidad.setToggleGroup(grupoRButtonCantidad);
        rbtnAgregarCantidad.setSelected(true);

    }

    public void cmbCodigoNombre_action(ActionEvent actionEvent) {
        Producto productoSeleccionado = cmbCodigoNombre.getValue();
        try{
            lblCantidadActual.setText(String.valueOf(productoSeleccionado.getCantidadTotal()));
            lblPrecioActual.setText(String.valueOf(productoSeleccionado.getPrecio()));
            lblDescuentoActual.setText(String.valueOf(productoSeleccionado.getDescuento()));
        }catch (Exception e){

        }

    }

    public void btnModificar_action(ActionEvent actionEvent) {
        Producto productoSeleccionado = cmbCodigoNombre.getValue();
        int cantidad=0;
        int precio;
        double descuento;

        try {
            try{
                if(rbtnCambiarCantidad.isSelected())
                    cantidad = Integer.parseInt(txtCantidad.getText());
                else if (rbtnAgregarCantidad.isSelected())
                    cantidad = Integer.parseInt(txtCantidad.getText()) + productoSeleccionado.getCantidadTotal();
            }catch (NumberFormatException nfe){
                cantidad = productoSeleccionado.getCantidadTotal();
            }
            try{
                precio = Integer.parseInt(txtPrecio.getText());
            }catch (NumberFormatException nfe){
                precio = productoSeleccionado.getPrecio();
            }
            try{
                descuento = Double.parseDouble(txtDescuento.getText());
            }catch (NumberFormatException nfe){
                descuento = productoSeleccionado.getDescuento();
            }

            Producto producto = new Producto(productoSeleccionado.getCodigo(),productoSeleccionado.getNombre(),cantidad,precio,descuento);

            AgregarHistoriaDeProducto(productoSeleccionado, producto);
            productoBsn.modificarProducto(producto);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Modificar Producto");
            alert.setHeaderText("Proceso correcto.");
            alert.setContentText("Producto modificado correctamente.");
            alert.showAndWait();

            limpiarCampos();
            initialize();

        }catch (NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eliminar producto");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText("Selecciona un producto en la primera opción");
            alert.showAndWait();
        }

    }

    private void AgregarHistoriaDeProducto(Producto productoNuevo, Producto productoViejo ) {
        String codigo = productoViejo.getCodigo();
        int unidadesAgregadas=0;
        int unidadesEliminadas=0;
        try{
            if(rbtnAgregarCantidad.isSelected()) unidadesAgregadas = Integer.parseInt( txtCantidad.getText() );
            else if(rbtnCambiarCantidad.isSelected()) {
                if( productoNuevo.getCantidadTotal()-productoViejo.getCantidadTotal() < 0  ){
                    unidadesEliminadas = ( productoNuevo.getCantidadTotal()-productoViejo.getCantidadTotal() )*(-1);
                }else {
                    unidadesAgregadas = productoNuevo.getCantidadTotal()-productoViejo.getCantidadTotal();
                }
            }
        }catch (NumberFormatException nfe){
            //nfe.printStackTrace();
        }

        int unidadesTotales = productoNuevo.getCantidadTotal();
        int precioDeCompraUnidad = 0;
        int precioDeVenta = productoNuevo.getPrecio();

        HistoriaDeProducto historiaDeProducto= new HistoriaDeProducto(codigo,unidadesAgregadas,
                            unidadesEliminadas,unidadesTotales,precioDeCompraUnidad,precioDeVenta);
        this.historiaDeProductoBsn.registrarHistoriaDeProducto(historiaDeProducto);
    }

    public void btnEliminar(ActionEvent actionEvent) {
        try {
            productoBsn.eliminarProducto(cmbCodigoNombre.getValue().getCodigo());
            limpiarCampos();
            initialize();
        }catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Eliminar producto");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText("Selecciona un producto en la primera opción");
            alert.showAndWait();
        }

    }
    private void limpiarCampos(){
        txtDescuento.clear();
        txtPrecio.clear();
        txtCantidad.clear();
        lblDescuentoActual.setText("");
        lblPrecioActual.setText("");
        lblCantidadActual.setText("");
        initialize();
    }

}
