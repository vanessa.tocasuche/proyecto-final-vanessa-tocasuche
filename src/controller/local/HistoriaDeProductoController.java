package controller.local;

import bsn.HistoriaDeProductoBsn;
import bsn.ProductoBsn;
import bsn.exception.ObjetoNoExisteException;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.HistoriaDeProducto;
import model.Producto;

import java.util.List;

public class HistoriaDeProductoController {
    @FXML
    private ComboBox<Producto> cmbCodigoNombre;
    @FXML
    private TableView<HistoriaDeProducto> tblProductos;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmFecha;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmUnidadesAgregadas;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmUnidadesSacadas;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmUnidadesTotales;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmPrecioCompra;
    @FXML
    private TableColumn<HistoriaDeProducto, String> clmPrecioVenta;

    private HistoriaDeProductoBsn historiaDeProductoBsn = new HistoriaDeProductoBsn();
    private ProductoBsn productoBsn = new ProductoBsn();

    @FXML
    private void initialize(){
        List<Producto> productos = this.productoBsn.listarProductos();
        ObservableList<Producto> productosObservables = FXCollections.observableList(productos);
        this.cmbCodigoNombre.setItems(productosObservables);

        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFechaDeSurtido().toString()));
        clmUnidadesAgregadas.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getUnidadesAgregadas())));
        clmUnidadesSacadas.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getUnidadesSacadas())));
        clmUnidadesTotales.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getUnidadesTotalesEnLocal())));
        clmPrecioCompra.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecioDeCompraDeUnidad())));
        clmPrecioVenta.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getPrecioDeVenta())));

    }


    public void cmbCodigoNombre_action() {
        try{
            List<HistoriaDeProducto> historiaDeProductos = this.historiaDeProductoBsn.listarHistoriaDeProducto(cmbCodigoNombre.getValue().getCodigo());
            ObservableList<HistoriaDeProducto> historiaObservable = FXCollections.observableList(historiaDeProductos);
            tblProductos.setItems(historiaObservable);

        }catch (ObjetoNoExisteException oyee){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Historia de producto");
            alert.setHeaderText("Ha ocurrido un error.");
            alert.setContentText(oyee.getMessage());
            alert.showAndWait();
        }

    }
}
