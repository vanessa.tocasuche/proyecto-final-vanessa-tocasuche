package model;

public class Local {
    //atributos
    private static String nit;
    private static String nombreDelLocal;
    private static String direccion;

    //constructor
    public Local() {
        nit = "100059818-96" ;
        nombreDelLocal = "SUPER ÚNICO";
        direccion = "Calle 20 D con carrera 5 en itagui";
    }

    //getters and setters
    public static String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        Local.nit = nit;
    }

    public String getNombreDelLocal() {
        return nombreDelLocal;
    }

    public void setNombreDelLocal(String nombreDelLocal) {
        Local.nombreDelLocal = nombreDelLocal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        Local.direccion = direccion;
    }
}
