package model;

import java.time.LocalDate;

public class Factura {
    //atributos
    private int numeroDeFactura;
    private LocalDate fechaDeVenta ;
    private String nombreEmpleado;
    private String IdDeCliente;
    private int totalAPagar;
    private Double descuento;
    private int totalAPagarConDescuento;


    //constructor
     public Factura(int numeroDeFactura, LocalDate fechaDeVenta, String nombreEmpleado) {
        this.numeroDeFactura= numeroDeFactura;
        this.fechaDeVenta = fechaDeVenta;
        this.nombreEmpleado = nombreEmpleado;
    }


//getter and setter

    public int getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public void setNumeroDeFactura(int numeroDeFactura) {
        this.numeroDeFactura = numeroDeFactura;
    }

    public LocalDate getFechaDeVenta() {
        return fechaDeVenta;
    }

    public void setFechaDeVenta(LocalDate fechaDeVenta) {
        this.fechaDeVenta = fechaDeVenta;
    }

    public int getTotalAPagar() {
        return totalAPagar;
    }

    public void setTotalAPagar(int totalAPagar) {
        this.totalAPagar = totalAPagar;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public int getTotalAPagarConDescuento() {
        return totalAPagarConDescuento;
    }

    public void setTotalAPagarConDescuento(int totalAPagarConDescuento) {
        this.totalAPagarConDescuento = totalAPagarConDescuento;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getIdDeCliente() {
        return IdDeCliente;
    }

    public void setIdDeCliente(String idDeCliente) {
        IdDeCliente = idDeCliente;
    }
}
