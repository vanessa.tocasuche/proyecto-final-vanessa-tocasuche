package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Producto {

    //atributos
    private String codigo;
    private String nombre;
    private int cantidadTotal;
    private int precio;
    private double descuento=0;

    //relaciones
    private List<HistoriaDeProducto> listaHistoriaDeProducto;

    //constructor
    public Producto(String codigo, String nombre, int cantidadTotal, int precio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cantidadTotal = cantidadTotal;
        this.precio = precio;
    }
    public Producto(String codigo, String nombre, int cantidadTotal, int precio, double descuento) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cantidadTotal = cantidadTotal;
        this.precio = precio;
        this.descuento = descuento;
    }


    //getters and setters

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadTotal() {
        return cantidadTotal;
    }

    public void setCantidadTotal(int cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public List<HistoriaDeProducto> getListaHistoriaDeProducto() {
        return listaHistoriaDeProducto;
    }

    public void setListaHistoriaDeProducto(List<HistoriaDeProducto> listaHistoriaDeProducto) {
        this.listaHistoriaDeProducto = listaHistoriaDeProducto;
    }

    @Override
    public String toString() {
        return codigo +" - "+nombre;    }
}
