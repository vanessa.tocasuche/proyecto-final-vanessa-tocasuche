package model.usuario;

import model.usuario.Cliente;

public class Empleado extends Persona {
    //atributos

    //constructor
    public Empleado(String id, String nombre, String contrasenia) {
        super(id, nombre, contrasenia);
    }

    @Override
    public String toString() {
        return String.format( getId() +" - " +getNombre());
    }

//getters and setters

}
