package model.usuario;

public class EmpleadoNormal extends Empleado{
    private char genero;
    private int edad;

    public EmpleadoNormal(String id, String nombre, String contrasenia, char genero, int edad) {
        super(id, nombre, contrasenia);
        this.genero = genero;
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
