package model.usuario;

public class Cliente extends Persona{
    //atributos
    private int comprasAlmes;
    private double descuento;

    //constructor
    public Cliente(String id, String nombre, String contrasenia) {
        super(id, nombre, contrasenia);
        this.comprasAlmes=0;
        this.descuento=0;
    }


    //getters and setters
    public int getComprasAlmes() {
        return comprasAlmes;
    }

    public void setComprasAlmes(int comprasAlmes) {
        this.comprasAlmes = comprasAlmes;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
}
