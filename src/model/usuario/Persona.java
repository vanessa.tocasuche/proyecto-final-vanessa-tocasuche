package model.usuario;


public class Persona {
    //atributos
    private String id;
    private String nombre;
    private String contrasenia;
    private static Persona personaLogueada;
    private static boolean estadoLogueo;

    //constructor
    public Persona(String id, String nombre, String contrasenia) {
        this.id = id;
        this.nombre = nombre;
        this.contrasenia = contrasenia;
    }


    //getters and setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public static Persona getPersonaLogueada() {
        return personaLogueada;
    }

    public static void setPersonaLogueada(Persona personaLogueada) {
        Persona.personaLogueada = personaLogueada;
    }

    public static boolean isEstadoLogueo() {
        return estadoLogueo;
    }

    public static void setEstadoLogueo(boolean estadoLogueo) {
        Persona.estadoLogueo = estadoLogueo;
    }

}
