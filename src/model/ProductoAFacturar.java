package model;

import java.util.ArrayList;
import java.util.List;

public class ProductoAFacturar {
    private String  numeroDeFactura;
    private Producto producto;
    private int cantidad;

    private static List<ProductoAFacturar> productosAFacturar = new ArrayList<>();

    public ProductoAFacturar(String numeroDeFactura, Producto producto, int cantidad) {
        this.numeroDeFactura = numeroDeFactura;
        this.producto = producto;
        this.cantidad = cantidad;
        addProductoAFacturarAList(this);
    }
    public void addProductoAFacturarAList(ProductoAFacturar este){
        productosAFacturar.add(este);
    }

    public String getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public void setNumeroDeFactura(String numeroDeFactura) {
        this.numeroDeFactura = numeroDeFactura;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public static List<ProductoAFacturar> getProductosAFacturar() {
        return productosAFacturar;
    }

    public static void setProductosAFacturar(List<ProductoAFacturar> productosAFacturar) {
        ProductoAFacturar.productosAFacturar = productosAFacturar;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
