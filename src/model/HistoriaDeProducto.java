package model;

import java.time.LocalDate;

public class HistoriaDeProducto {
    private String codigo;
    private LocalDate fechaDeSurtido;
    private int unidadesAgregadas;
    private int unidadesSacadas;
    private int unidadesTotalesEnLocal;
    private int precioDeCompraDeUnidad;
    private int precioDeVenta;

    //constructor
    public HistoriaDeProducto(String codigo, int unidadesAgregadas, int unidadesSacadas, int unidadesTotalesEnLocal, int precioDeCompraDeUnidad, int precioDeVenta) {
        this.codigo = codigo;
        this.fechaDeSurtido = LocalDate.now();
        this.unidadesAgregadas = unidadesAgregadas;
        this.unidadesSacadas = unidadesSacadas;
        this.unidadesTotalesEnLocal = unidadesTotalesEnLocal;
        this.precioDeCompraDeUnidad = precioDeCompraDeUnidad;
        this.precioDeVenta = precioDeVenta;
    }

    //getters and setters

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public LocalDate getFechaDeSurtido() {
        return fechaDeSurtido;
    }

    public void setFechaDeSurtido(LocalDate fechaDeSurtido) {
        this.fechaDeSurtido = fechaDeSurtido;
    }

    public int getUnidadesAgregadas() {
        return unidadesAgregadas;
    }

    public void setUnidadesAgregadas(int unidadesAgregadas) {
        this.unidadesAgregadas = unidadesAgregadas;
    }

    public int getUnidadesSacadas() {
        return unidadesSacadas;
    }

    public void setUnidadesSacadas(int unidadesSacadas) {
        this.unidadesSacadas = unidadesSacadas;
    }

    public int getUnidadesTotalesEnLocal() {
        return unidadesTotalesEnLocal;
    }

    public void setUnidadesTotalesEnLocal(int unidadesTotalesEnLocal) {
        this.unidadesTotalesEnLocal = unidadesTotalesEnLocal;
    }

    public int getPrecioDeCompraDeUnidad() {
        return precioDeCompraDeUnidad;
    }

    public void setPrecioDeCompraDeUnidad(int precioDeCompraDeUnidad) {
        this.precioDeCompraDeUnidad = precioDeCompraDeUnidad;
    }

    public int getPrecioDeVenta() {
        return precioDeVenta;
    }

    public void setPrecioDeVenta(int precioDeVenta) {
        this.precioDeVenta = precioDeVenta;
    }
}
